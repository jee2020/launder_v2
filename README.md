# Launder Project

## Features available

* Back

## Technologies used

* MongoDB
* Java 11 (Lombok, JUnit)
* Spring Data MongoDB

## Build the project

Download the project `git clone git@gitlab.com:jee2020/launder.git Launder`

Move in the directory `cd Launder`

Install the dependencies `mvn install`