package com.example.launder;

import com.example.launder.customers.baskets.services.BasketService;
import com.example.launder.orders.DTO.OrderDTO;
import com.example.launder.orders.models.Order;
import com.example.launder.orders.services.OrderService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class OrderServiceTest {

    @InjectMocks
    private OrderService orderService;

    // TESTING METHOD 1
    @Test
    void should_create_an_order(){
        // TODO: create an order according to createOrder method in OrderService


    }

    // TESTING : METHOD 2 -- OK
    @Test
    void should_create_a_DTO_order(){
        Order order = new Order();
        assertThat(orderService.OrderToDTO(order), instanceOf(OrderDTO.class));
    }

    // TESTING : METHOD 3 -- OK
    @Test
    void should_get_delivery_price(){
        Order order = new Order();

        order.setPriceDelivery(40);

        assertThat(order.getPriceDelivery(), greaterThan(0));
        assertThat(order.getPriceDelivery(), instanceOf(Integer.class));
    }

    // TESTING : METHOD 4
    @Test
    void should_get_an_order(){
        //TODO suppposed to return an object
        Order order = new Order();


    }


}
