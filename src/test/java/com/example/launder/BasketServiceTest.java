package com.example.launder;


import com.example.launder.customers.baskets.DTO.BasketDTO;
import com.example.launder.customers.baskets.models.Basket;
import com.example.launder.customers.baskets.services.BasketService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


@SpringBootTest
public class BasketServiceTest {

    @InjectMocks
    private BasketService basketService;

    @Value("${app.basket.content.weight}")
    private Integer weight;


    @Test
    void should_get_a_price(){
        Basket basket = new Basket();
        basket.setWeight(200);
        basket.setPrice(basket.getWeight()*this.weight);
        assertThat(basket).hasFieldOrProperty("price");
        assertThat(basket.getPrice(), greaterThan(0));
        assertThat(basket.getPrice(), instanceOf(Integer.class));
    }

    @Test
    void should_create_a_DTO_basket(){
        Basket basket = new Basket();
        assertThat(basketService.BasketToDTO(basket), instanceOf(BasketDTO.class));
    }

}
