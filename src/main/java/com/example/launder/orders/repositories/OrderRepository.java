package com.example.launder.orders.repositories;

import com.example.launder.customers.baskets.models.Basket;
import com.example.launder.orders.models.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface OrderRepository extends MongoRepository<Order, String> {
    @Query("{ _id : ?0 }")
    Order findBy_id(String _id);

    List<Order> findByUserIdOrderByUpdatedTimestampDesc(String userId);
}
