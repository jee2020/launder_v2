package com.example.launder.orders.models;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
public class Order {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    private String userId;
    /**
     * State -2 = Commande annulée
     * State -1 = Commande en pause
     * State 0 = Commande créée
     * State 1 = Cleaner Accepted
     * State 2 = Picker accepted
     * State 3 = Basket picked up
     * State 4 = Cleaner working
     * State 5 = Basket Ready for delivery
     * State 6 = Basket in delivery
     * State 7 = Basket delivered
     */
    private Integer state;
    private String pickerId;
    private String deliverId;
    private String cleanerId;
    private Integer basketWeight;
    private Integer type;
    private Integer priceTotal;
    private Integer priceDelivery;
    private Long createdTimestamp;
    private Long updatedTimestamp;
}
