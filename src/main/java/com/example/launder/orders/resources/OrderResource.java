package com.example.launder.orders.resources;

import com.example.launder.customers.baskets.repositories.BasketRepository;
import com.example.launder.customers.baskets.services.BasketService;
import com.example.launder.orders.models.Order;
import com.example.launder.orders.repositories.OrderRepository;
import com.example.launder.orders.services.OrderService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderResource {

    @Autowired
    OrderRepository orderRepository;

    private final OrderService orderService;

    public OrderResource(OrderService orderService) { this.orderService = orderService; }

    @GetMapping("/{orderId}")
    public ResponseEntity<?> getOrder(@PathVariable String orderId){
        Order order = orderService.getOrder(orderId);
        if(order != null){
            return new ResponseEntity<>(orderService.OrderToDTO(order), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/history/{userId}")
    public ResponseEntity<?> getHistoryOrders(@PathVariable String userId){
        List<Order> ordersHistory= orderRepository.findByUserIdOrderByUpdatedTimestampDesc(userId);
        return new ResponseEntity<>(ordersHistory, HttpStatus.FOUND);
    }

    @PostMapping("/{userId}")
    public ResponseEntity<?> createOrder(@PathVariable String userId, @RequestBody Integer type){
        if(type == null){
            return new ResponseEntity<>("NOK", HttpStatus.NOT_ACCEPTABLE);
        }
        Order order = orderService.createOrder(userId, type);
        if(order != null){
            orderRepository.save(order);
            order.set_id(order.get_id());
            return new ResponseEntity<>(orderService.OrderToDTO(order), HttpStatus.CREATED);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping("/{orderId}")
    public ResponseEntity<?> updateStateOrder(@PathVariable String orderId, @RequestBody Integer state){
        Order order = orderService.getOrder(orderId);
        if(order != null){
            order.setState(state);
            orderRepository.save(order);
            return new ResponseEntity<>(order, HttpStatus.OK);
        }else{
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
