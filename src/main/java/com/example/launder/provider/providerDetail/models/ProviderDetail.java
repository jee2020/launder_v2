package com.example.launder.provider.providerDetail.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
public class ProviderDetail {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    private String providerId;
    private String startingHourAM;
    private String endingHourAM;
    private String startingHourPM;
    private String endingHourPM;
    private String phone;
    private String addressId;
}
