package com.example.launder.provider.providerDetail.repositories;

import com.example.launder.provider.providerDetail.models.ProviderDetail;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProviderDetailRepository extends MongoRepository<ProviderDetail, String> {
    List<ProviderDetail> findByProviderId(@Param("providerId") String providerId);
}
