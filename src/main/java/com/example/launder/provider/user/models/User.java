package com.example.launder.provider.user.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
public class User {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    private String lastName;
    private String firstName;
    private String password;
    private String email;
    private String phone;
    private boolean active;
}
