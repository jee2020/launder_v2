package com.example.launder.provider.user.DTO;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class UserDTO {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    @NotBlank
    private String lastName;
    @NotBlank
    private String firstName;
    @NotBlank
    private String password;
    @Email
    private String email;
    @Pattern(regexp="(^$|[0-9]{10})")
    private String phone;
    private boolean active;
}
