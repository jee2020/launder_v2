package com.example.launder.provider.address.models;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
public class Address {
    @MongoId(value= FieldType.OBJECT_ID)
    private String _id;
    private String userId;
    private String name;
    private int streetNumber;
    private String streetName;
    private int zipCode;
    private String city;
}
