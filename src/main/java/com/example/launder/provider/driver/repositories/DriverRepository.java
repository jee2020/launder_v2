package com.example.launder.provider.driver.repositories;

import com.example.launder.provider.driver.models.Driver;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DriverRepository extends MongoRepository<Driver, String>{
    List<Driver> findAllByAvailable(@Param("available") boolean available, Pageable page);
    Driver findByUserId(@Param("userId") String userId);
}
