package com.example.launder.provider.driver.models;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
public class Driver {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    private String userId;
    private boolean available;
    private boolean waitingForConfirmation;
}
