package com.example.launder.provider.driver.services;

import com.example.launder.commons.exceptions.ConflictException;
import com.example.launder.orders.models.Order;
import com.example.launder.orders.services.OrderService;
import com.example.launder.provider.driver.models.Driver;
import com.example.launder.provider.driver.repositories.DriverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.BooleanOperators;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DriverService {
    @Autowired
    DriverRepository driverRepository;
    @Autowired
    OrderService orderService;

    public void updateConfirmationStatus(List<Driver> drivers) {
        for (Driver driver:
             drivers) {
            driver.setWaitingForConfirmation(true);
        }
        driverRepository.saveAll(drivers);
    }

    public void updateAvailable(Driver driver) throws ConflictException {
        driver.setAvailable(false);
        driverRepository.save(driver);
    }

    public void updateConfirmation(Driver driver) throws ConflictException {
        driver.setWaitingForConfirmation(false);
        driverRepository.save(driver);
    }

    public void orderAccepted(Driver driver, String orderId) {
        Order order = orderService.getOrder(orderId);
        orderService.updateOrderStateDriver(order, 2, driver.get_id());
    }

    public Order orderPickedUp(Driver driver, String orderId) {
        Order order = orderService.getOrder(orderId);
        if (driver.get_id().equals(order.getPickerId())) {
            orderService.updateOrderStateDriver(order, 3, driver.get_id());
        }
        return order;
    }

    public Order orderDeliveredCleaner(Driver driver, String orderId) {
        Order order = orderService.getOrder(orderId);
        if (driver.get_id().equals(order.getPickerId())) {
            orderService.updateOrderStateDriver(order, 4, driver.get_id());
        }
        return order;
    }

    public Order orderDonePickedUp(Driver driver, String orderId) {
        Order order = orderService.getOrder(orderId);
        orderService.updateOrderStateDriver(order, 6, driver.get_id());
        return order;
    }

    public Order orderDoneDelivered(Driver driver, String orderId) {
        Order order = orderService.getOrder(orderId);
        if (driver.get_id().equals(order.getPickerId())) {
            orderService.updateOrderStateDriver(order, 7, driver.get_id());
        }
        return order;
    }
}
