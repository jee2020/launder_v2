package com.example.launder.customers.baskets.DTO;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Data
public class BasketDTO {
    @MongoId(value = FieldType.OBJECT_ID)
    private String _id;
    private String userId;
    private Integer weight;
    private Integer price;
    private Long createdTimestamp;
}
