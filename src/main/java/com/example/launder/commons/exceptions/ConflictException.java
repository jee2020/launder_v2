package com.example.launder.commons.exceptions;

public class ConflictException extends Exception {
    public ConflictException(String message) {
        super(message);
    }
}
