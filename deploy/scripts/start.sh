#!/bin/bash

HOME=/home/ec2-user
APP=$HOME/launder
CMD=$APP/launder.sh
LOG=$HOME/deploy.log

/bin/echo "$(date '+%Y-%m-%d %X'): ** Application Start Hook Started **" >> $LOG
/bin/echo "$(date '+%Y-%m-%d %X'): Event: $LIFECYCLE_EVENT" >> $LOG

pwd >> $LOG
cd $APP

if [ -f $CMD ]
then
    echo $APP >> $LOG
    pwd >> $LOG
    DB_PASS=DB_PASSWORD $CMD start
    /bin/echo "$(date '+%Y-%m-%d %X'): Starting $APPLICATION_NAME" >> $LOG
else
    /bin/echo "$(date '+%Y-%m-%d %X'): $CMD not found. Proceeding with deployment" >> $LOG
fi
/bin/echo "$(date '+%Y-%m-%d %X'): ** Application Start Hook Completed **" >> $LOG
